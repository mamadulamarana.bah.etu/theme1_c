all: test

numbers-test: numbers-test.o
	gcc -o numbers-test numbers-test.o

numbers-test.o: numbers-test.c
	gcc -c numbers-test.c

test: numbers-out.txt numbers-test.txt
	diff -u numbers-test.txt numbers-out.txt

clean:
	rm -f *.o numbers-test tmp

.PHONY: all test clean
