#include <stdio.h>

/**
 * Exo17
*/
int put_digit(int digit) {
    if (digit >= 0 &&  digit < 10) {
        putchar(digit+'0');
        return 0;
    }
    return -1;
}

/**
 * Exo18
*/
int put_hdigit(int h) {
    if (h >= 10 && h < 16) {
        putchar(h + 'A' - 10);
        return 0;
    }
    return put_digit(h);
}

/**
 * Exo 19
*/
int putdecP(int d) {
    if ( d > 10) {
        int r = d%10;
        int q = d/10;
        putdecP(q);
        put_digit(r);
        return 0;
    }
    else {
        return put_digit(d);
    }
}

/**
 * Exo 20
*/
int putdec(int d) {
    if( d == -2147483648 || d == 2147483648) {
        int r = d%10;
        int q = d/10;
        r = -r;
        putdec(q);
        return put_digit(r);
    }

    else if ( d < 0) {
        d = -d;
        putchar('-');
        return putdecP(d);
    }
    else {
        return putdecP(d);
    }
}

/**
 * Exo 21
*/
int putbin(unsigned int b) {
    if ( (b >> 1)) {
        int c = putbin(b >> 1);
        if (c) {
            return c;
        }
    }
    return put_digit(b & 1);
}

/**
 * exo 22
*/
int puthex(int h){
 
    unsigned int m = (unsigned int )h;
    if (m >> 4){
        int c = puthex(m >> 4);
        if (c) {
            return c;
        }
    }
    return put_hdigit(h & 0b1111);
}

    int newline() {
        putchar('\n');
        return 0;
    }


int put_test_line(int n)
{
    putchar('t');
    putchar('e');
    putchar('s');
    putchar('t');
    putchar(' ');
    putchar('#');
    putdec(n);
    putchar(':');

    return 0;
}


int main()
{ 
    put_test_line(1); putdec(214); newline();
    put_test_line(2); putdec(-74); newline();
    put_test_line(3); putdec(1); newline();
    put_test_line(4); putdec(-1); newline();
    put_test_line(5); putdec(0); newline();
    put_test_line(6); putdec(2147483647); newline();
    put_test_line(7); putdec(-2147483648); newline();
    put_test_line(8); putdec(-(-2147483648)); newline();
    put_test_line(9); puthex(0); newline();
    put_test_line(10); puthex(10); newline();
    put_test_line(11); puthex(16); newline();
    put_test_line(12); puthex(2147483647); newline();
    put_test_line(13); puthex(-2147483648); newline();
    put_test_line(14); puthex(0xCAFEBABE); newline();
    put_test_line(15); puthex(-1); newline();
    put_test_line(16); putbin(0); newline();
    put_test_line(17); putbin(1); newline();
    put_test_line(18); putbin(-1); newline();
    put_test_line(19); putbin(2147483647); newline();
    put_test_line(20); putbin(-2147483648); newline();
    return 0;
}
