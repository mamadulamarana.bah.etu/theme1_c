# Noms :
  # BAH MAMADU LAMARANA

# TP d'introduction à la pratique du langage C

Ce dépôt correspond au premier TP de PDC, dont le sujet est à https://www.fil.univ-lille.fr/~ballabriga/bpc/tdtp/tp_theme1.html


##  Instructions pour rendre votre travail avec gitlab

Pour permettre à votre chargé de TD de suivre votre travail sur ce projet :

* *forkez* ce dépôt (bouton _Fork_ ou _Fourcher_, _Créer une divergence_),
  - choissisez la visibilité _privé_, _private_, pour ce projet 
* dans le dépôt *forké*, ajoutez votre binôme et votre chargé de TD aux
  membres du projet avec l’accès _Developer_,
* éditez ce fichier `Readme.md` pour indiquer vos noms (membres du
  binôme) et supprimer ce paragraphe d’instructions.

Voyez les instructions précises dans le sujet de TP. 

## Contenu de ce dépôt

* `prog1.c` premier programme, à indenter
* `numbers-*` section _Différents sens pour les mêmes nombres_ du sujet 
* `prepro/` section _Le préprocesseur_ du sujet 
* `module/` section _Compilation modulaire_ du sujet
* `mcu/` section _Mes commandes Unix_ du sujet 



